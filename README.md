# WebScript

Utilidades para TypeScript e JavaScript voltados para desenvolvimento web.

O código-fonte...
- ... está armazenado e versionado nas pastas `ts` e `js`, para TypeScript e JavaScript respectivamente.
- ... na pasta JavaScript não é escrito manualmente, mas apenas compilado pelo *tsc*, o TypeScript Compiler.
- ... é distribuido na última versão possível do TypeScript e JavaScript.
